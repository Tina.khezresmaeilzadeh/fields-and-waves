%% Question 1

Z0 = 50;
ZG = 30;
R = 40;
f = 30*10^6 ;
w = 2* pi * f;
L = 20*10^(-6);
C = 10*10^(-9);
Beta = w/(3*10^8);
ZL = R + 1i*w*L + 1/(1i*w*C);

GammaG = (ZG - Z0)/(ZG + Z0);
GammaL = ( ZL - Z0 )/( ZL + Z0);

l =100;

z = 0 : l/1000: l;
VG = 1;
Zl = Z0*( ZL + Z0*tanh(1i.*Beta.*z) )./( Z0 + ZL*tanh(1i.*Beta.*z) );
Gammal = (Zl - Z0)./(Zl + Z0);
SWR = (1 + abs(( Zl -  Z0 )./(Zl + Z0)) ) ./( 1 - abs(( Zl -  Z0 )./(Zl + Z0)) );
SWRmean = mean(SWR);
Vl = Z0*VG.*exp(-1i.*Beta.*l).*( 1+ Gammal)./((Z0 + ZG).*(1 - GammaG.*GammaL.*exp(-1i*2*Beta*l)));

plot(z, abs(Vl));
grid on
xlabel('z');
ylabel('Vl(z)');
title('Wave graph');

%% Question 2

%definitions
Z0 = 50;
ZL = 150;
ZG = 30;
T = 1;
c = 1; 
d = T*c;
dl = d/100;
l = 0:dl:d;
dt =dl/c;
t = 0:dt:10*T;

% calculating VG
VG = zeros(length(t));
for i=1:length(VG)
    if t(i) < T/5
        VG(i) = 1;
    else
        VG(i)= 0;
    end
end

% Calculating V
V = VG.*Z0./(ZG + Z0);
h = figure;
axis tight manual
filename = 'Question2FirstPart.gif';

% Reflection Coefficients
GammaG = (ZG - Z0)/(ZG + Z0);
GammaL = (ZL - Z0)/(ZL + Z0);
%Plotting part
for timeCounter = 1:size(t, 2)
    % creating Vl vector
    Vl = zeros(size(l));
    %obtaining Vl
    for lengthCounter = 1:size(l, 2)
        sum1 = 0;
        sum2 = 0;
        for m = 0:floor((t(timeCounter) - l(lengthCounter)/c)/2/T)
            a = int32((t(timeCounter) - l(lengthCounter)/c - 2*m*T)/dt) + 1;
            sum1 = sum1 + ((GammaG*GammaL)^(m))*V(a);
        end
        for k = 0:floor((t(timeCounter) + l(lengthCounter)/c - 2*T)/2/T)
            a = int8((t(timeCounter) + l(lengthCounter)/c - 2*(k + 1)*T)/dt) + 1;
            sum2 = sum2 + (((GammaG*GammaL)^(k))*V(a)*GammaL);
        end
        Vl(lengthCounter) = sum1 + sum2;
        
    end
    
    plot(l,Vl);
    xlabel("z")
    ylabel("V(z)")
    title("Not Lossy transmission line ")
    grid on
    ylim([-1 1])
    drawnow
    
    % save
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    if i == 1 
      imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',dt/10); 
    else 
      imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',dt/10); 
    end
end

%% Lossy Condition

%definitions
Z0 = 50;
ZL = 150;
ZG = 30;
R = 10;
alpha = R/Z0;
T = 1;
c = 1; 
d = T*c;
dl = d/100;
l = 0:dl:d;
dt =dl/c;
t = 0:dt:10*T;
% calculating VG
VG = zeros(length(t));
for i=1:length(VG)
    if t(i) < T/5
        VG(i) = 1;
    else
        VG(i)= 0;
    end
end

% Calculating V
V = VG.*Z0./(ZG + Z0);
h = figure;
axis tight manual
filename = 'Question2FirstPart.gif';

% Reflection Coefficients
GammaG = (ZG - Z0)/(ZG + Z0);
GammaL = (ZL - Z0)/(ZL + Z0);
%Plotting part
for timeCounter = 1:size(t, 2)
    % creating Vl vector
    Vl = zeros(size(l));
    %  setting the z values
    if(mod(floor(t(timeCounter)/T),2) == 1)
        Zpass = (floor(t(timeCounter)/T)+1)*d;
         sgn = -1;
    else
        Zpass = floor(t(timeCounter)/T)*d;
        sgn = 1;
    end
    
    %obtaining Vl
    for lengthCounter = 1:size(l, 2)
        sum1 = 0;
        sum2 = 0;
        
        for m = 0:floor((t(timeCounter) - l(lengthCounter)/c)/2/T)
            a = int32((t(timeCounter) - l(lengthCounter)/c - 2*m*T)/dt) + 1;
            sum1 = sum1 + (exp((-1)*alpha*(Zpass + sgn*l(lengthCounter))))*((GammaG*GammaL)^(m))*V(a);
        end
        for k = 0:floor((t(timeCounter) + l(lengthCounter)/c - 2*T)/2/T)
            a = int8((t(timeCounter) + l(lengthCounter)/c - 2*(k + 1)*T)/dt) + 1;
            sum2 = sum2 + (exp((-1)*alpha*(Zpass + sgn*l(lengthCounter))))*(((GammaG*GammaL)^(k))*V(a)*GammaL);
        end
        Vl(lengthCounter) = sum1 + sum2;
        
    end
    
    plot(l,Vl);
    xlabel("z")
    ylabel("V(z)")
    title("Not Lossy transmission line ")
    grid on
    ylim([-1 1])
    drawnow
    
    % save
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    if i == 1 
      imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',dt/10); 
    else 
      imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',dt/10); 
    end
end
%% Laplace
C = 100;
L = 100;
R = 150;
ZG = 50;
Z0 = 50;
T = 1;
c = 1;
d = T*c;
dl = d/100;
l = 0:dl:d;
dt =dl/c;

syms s t
GL = 1/R + 1/(s*L) + (s*C) ;
ZL = 1/GL;
GammaL = (ZL - Z0 )/(ZL + Z0);
GammaD = GammaL*exp(-s*T);

Vd =  Z0*(1 + GammaD )/(s*(Z0 + ZG));
VL =  Z0*(1 + GammaL )/(s*(Z0 + ZG)) * exp(-s*T);

VL(t) = ilaplace(VL);
Vd(t) = ilaplace(Vd);

% plot VL(t)
figure
fplot(VL(t))

title("VL(t) step response")
xlim([0 10*T]);
xlabel("t")
ylabel("VL")
grid on

% plot Vd(t)
figure
fplot(Vd(t))
xlim([0 10*T]);
title("Vd(t) step response")
xlabel("t")
ylabel("Vd")
grid on

%% Question 3
% part one
% raveshe 1
syms x1 x2
eqn = x2*( ZL + x2*tanh(1i*Beta*x1) )/( x2 + ZL*tanh(1i*Beta*(x1))) == 50;
eqns = [real(eqn) == 50, imag(eqn) == 0];
vars = [x1 x2];
[solv, solu] = solve(eqns, vars);
%%
% part 1
% raveshe 2
min = 1000;

for Z1 = 10^(-2):0.01:100
    for L1 = 10^(-2):0.01:30
        ZLnew = Z1*( ZL + Z1*tanh(1i.*Beta.*L1) )./( Z1 + ZL.*tanh(1i.*Beta.*L1) );
        if abs(real( ZLnew ) - 50) + abs(imag(ZLnew)) < min
            L1answer = L1;
            Z1answer = Z1;
            min = abs(real( ZLnew ) - 50) ;
        end
    end
end

%%
% plot reflection coefficient with one of the Z1 and L1 values

Z0 = 50;
Zg = 30;
R = 40;
%f = 1*10^6:1000: 100*10^6;
f = 30 * 10^(6);
w = 2* pi * f;
L = 20*10^(-6);
C = 10*10^(-9);
ZL = R + 1i*w*L + 1./(1i*w*C);
VelocityFactor = 0.8;
Beta = w /(3*10^8 * VelocityFactor);
Gamma = (ZLnew - Z0)./(ZLnew + Z0);

figure
plot(f, abs(Gamma));
grid on
xlabel('frequency');
ylabel('Abs Of Reflectoion Coefficient');
title('First Model Reflectoion Coefficient');
figure
plot(f, angle(Gamma));
grid on
xlabel('frequency');
ylabel('Angle Of Reflectoion Coefficient');
title('First Model Reflectoion Coefficient');

%%
% Two-section series impedance transformer

Z0 = 50;
Zg = 30;
R = 40;
f = 30*10^6;
w = 2* pi * f;
L = 20*10^(-9);
C = 10*10^(-9);
ZL = R + 1i*w*L + 1./(1i*w*C);
VelocityFactor = 0.8;
Beta = w /(3*10^8 * VelocityFactor);
Z1 = 70;
Z2 = 60;

min = 100;
for L2= 10^(-2): 0.01: 10
    
    for  L1= 10^(-2): 0.01: 10
        ZL2 = Z2*( (ZL + Z2*1i*tan(Beta*L2))/(Z2 + ZL*1i*tan(Beta*L2)) );
        ZL1 =  Z1*( (ZL2 + Z1*1i*tan(Beta*L1))/(Z1 + ZL2*1i*tan(Beta*L1)) );
        if abs(ZL1  - Z0 ) < min
            ZL1Matched = ZL1;
            min = abs(ZL1  - Z0 );
            L1min = L1;
            L2min = L2;
            ZLans = ZL1;
        end    
    end
   
end

%%
Z0 = 50;
Zg = 30;
R = 40;
f = 1*10^6:1000: 100*10^6;
w = 2* pi * f;
L = 20*10^(-9);
C = 10*10^(-9);
ZL = R + 1i*w*L + 1./(1i*w*C);
VelocityFactor = 0.8;
Beta = w /(3*10^8 * VelocityFactor);
Z1 = 70;
Z2 = 60;

L1 = 2.8100;
L2 = 1.3100;

ZL2 = Z2* (ZL + Z2*1i*tan(Beta*L2)  )./(Z2 + ZL.*1i.*tan(Beta*L2)) ;
ZL1 =  Z1*(ZL2 + Z1*1i*tan(Beta*L1))./(Z1 + ZL2*1i.*tan(Beta*L1)) ;
Gamma = (ZL1 - Z0)./(ZL1 + Z0);

figure
plot(f, abs(Gamma));
grid on
xlabel('frequency');
ylabel('Abs Of Reflectoion Coefficient');
title('Second Model Reflectoion Coefficient');
figure
plot(f, angle(Gamma));
grid on
xlabel('frequency');
ylabel('Angle Of Reflectoion Coefficient');
title('Second Model Reflectoion Coefficient');
%% part 3

Z0 = 50;
Zg = 30;
R = 40;
f = 30*10^6;
w = 2* pi * f;
L = 20*10^(-9);
C = 10*10^(-9);
ZL = R + 1i*w*L + 1./(1i*w*C);
VelocityFactor = 0.8;
Beta = w /(3*10^8 * VelocityFactor);
Lambda0 = 2*pi/(0.8*Beta);

l1 = Lambda0/3;
l2 = Lambda0/3;

min = 1000;
for d1 = 0.01:0.1:20
    for d2 = 0.01:0.1:20
        for d3 = 0.01:0.1:20
            
            Z3 = 1i*Z0*tan(Beta*d3);
            Zc = ZL*Z3/(ZL + Z3);
            Z2 = Z0*(Zc + Z0*1i*tan(Beta*l2))/(Z0 + Zc*1i*tan(Beta*l2));
            Z2prime = 1i*Z0*tan(Beta*d2);
            Zb = Z2*Z2prime/(Z2 + Z2prime);
            Z1 = Z0*(Zb + Z0*1i*tan(Beta*l1))/(Z0 + Zb*1i*tan(Beta*l1));
            Z1prime = 1i*Z0*tan(Beta*d1);
            Za = Z1*Z1prime/(Z1 + Z1prime);
            
            if abs(Za  - Z0 ) < min
                ZaMatched = Za;
                min = abs(Za  - Z0 );
                d1min = d1;
                d2min = d2;
                d3min = d3;
            end
            

            
            
        end
    end
end
%%
Z0 = 50;
Zg = 30;
R = 40;
f = 1*10^6:1000: 100*10^6;
w = 2* pi * f;
L = 20*10^(-9);
C = 10*10^(-9);
ZL = R + 1i*w*L + 1./(1i*w*C);
VelocityFactor = 0.8;
Beta = w /(3*10^8 * VelocityFactor);
Lambda0 = 2*pi./(0.8*Beta);

l1 = Lambda0/3;
l2 = Lambda0/3;

d1 = 17.61;
d2 = 10.01;
d3 = 13.81;


Z3 = 1i*Z0*tan(Beta*d3);
Zc = ZL.*Z3./(ZL + Z3);           
Z2 = Z0.*(Zc + Z0*1i.*tan(Beta.*l2))./(Z0 + Zc*1i.*tan(Beta.*l2));            
Z2prime = 1i*Z0*tan(Beta*d2);            
Zb = Z2.*Z2prime./(Z2 + Z2prime);           
Z1 = Z0.*(Zb + Z0.*1i.*tan(Beta.*l1))./(Z0 + Zb*1i.*tan(Beta.*l1));            
Z1prime = 1i.*Z0*tan(Beta.*d1);            
Za = Z1.*Z1prime./(Z1 + Z1prime);

Gamma = (Za - Z0)./(Za + Z0);

figure
plot(f, abs(Gamma));
grid on
xlabel('frequency');
ylabel('Abs Of Reflectoion Coefficient');
title('Second Model Reflectoion Coefficient');
figure
plot(f, angle(Gamma));
grid on
xlabel('frequency');
ylabel('Angle Of Reflectoion Coefficient');
title('Second Model Reflectoion Coefficient');
%% Question 4
% Line Step Responce
Z0 = 50;
ZG = 60;
R = 40;
f = 30*10^6 ;
w = 2* pi * f;
L = 20*10^(-9);
C = 10*10^(-9);
Beta = w/(3*10^8);
d = 1.5*(2*pi/(Beta*4));
T = 1;
syms s t
ZL = 1i*Z0*tan(Beta*d);
GammaL = (ZL - Z0 )/(ZL + Z0);
GammaD = GammaL*exp(-s*T);
GammaG = (ZG - Z0)/(ZG + Z0);

Vd =  Z0*(1 + GammaD )/(s*(Z0 + ZG)*(1 - GammaG*GammaD));
vd(t) = ilaplace(Vd);
fplot(vd(t));
title("Vd(t) step response")
xlabel("t")
ylabel("Vd")
grid on
%% Voltage Capacitor Step Responce
syms s t
R = 20;
C = 10*10^(-3);
Zc = 1/(s*C);
Vcap = Zc/(s*(Zc + R ));
vcap(t) =  ilaplace(Vcap);
fplot(vcap(t));
title("Vcap(t) step response")
xlabel("t")
ylabel("Vcap")
grid on
%% Transmission Line Reactance
Z0 = 50;
ZG = 30;
R = 40;
%f = 1*10^6:10000: 100*10^5;
f = linspace(10^(6),10^(8),1001);
w = 2* pi * f;
L = 20*10^(-9);
C = 10*10^(-9);
Beta = w./(3*10^8);
d = 10;
XL = Z0.*tan(Beta.*d);
figure
plot(f, XL);
grid on
xlabel('frequency');
ylabel('Reactance');
title('Reactanse Of Transmission Line');
%% Inductor Reactance
f = 1*10^6:1000: 100*10^6;
w = 2* pi * f;
L = 20*10^(-9);
XL = w.*L;
figure
plot(f, XL);
grid on
xlabel('frequency');
ylabel('Reactance');
title('Reactanse Of Inductor');
%% Capacitor Reactance
f = 1*10^6:1000: 100*10^6;
w = 2* pi * f;
C = 10*10^(-9);
XL = -1./(w.*C);
figure
plot(f, XL);
grid on
xlabel('frequency');
ylabel('Reactance');
title('Reactanse Of Capacitor');
%% RC Reactance
f = 1*10^6:1000: 100*10^6;
w = 2* pi * f;
C = 10*10^(-9);
Zc = 1./(1i.*w.*C);
R = 10;
ZL = Zc.*R./(Zc + R);
XL = imag(ZL);
figure
plot(f, XL);
grid on
xlabel('frequency');
ylabel('Reactance');
title('Reactanse Of RC Circuit');